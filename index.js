// Declare const of area
const A_Area = "a";
const B_Area = "b";
const C_Area = "c";
const not_Area = "x";

// Declare const of object
const O1_Object = "1";
const O2_Object = "2";
const O3_Object = "3";
const not_Object = "0";

document.getElementById("clickShow1").onclick = function () {
  // INPUT
  // Declare available variables which user input
  var standardBoards = document.getElementById("boardStandardValue").value * 1;
  var area = document.getElementById("area-select").value;
  var object = document.getElementById("object-select").value;
  var score1 = document.getElementById("1SubjectValue").value * 1;
  var score2 = document.getElementById("2SubjectValue").value * 1;
  var score3 = document.getElementById("3SubjectValue").value * 1;

  // PROCESS
  var total;
  total =
    score1 + score2 + score3 + areaOfStudent(area) + objectOfStudent(object);

  // RESULT
  // show result on the screen
  var result = `<p><i class="fa fa-caret-right"></i> Student was ${failOrPass(
    total,
    standardBoards
  )} with total score ${total} </p>`;
  document.getElementById("resultBT1").innerHTML = result;
};

// Function to identify area of each object
function areaOfStudent(a) {
  switch (a) {
    case A_Area: {
      return 2;
    }
    case B_Area: {
      return 1;
    }
    case C_Area: {
      return 0.5;
    }
    case not_Area: {
      return 0;
    }
  }
}

// Function to identify object
function objectOfStudent(o) {
  switch (o) {
    case O1_Object:
      return 2.5;
    case O2_Object:
      return 1.5;
    case O3_Object:
      return 1;
    case not_Object:
      return 0;
  }
}

// Determine fail or pass
function failOrPass(a, b) {
  if (a < b) return "failed";
  else return "passed";
}

// ===================== EX 2 ==============================
document.getElementById("clickShow2").onclick = function () {
  // INPUT
  // Declare available variables: name and electricity number
  var fullName = document.getElementById("fullNameValue").value;
  var electricityKW = document.getElementById("electricityValue").value * 1;

  // RESULT
  // show result on the screen
  var result2 = `<p><i class="fa fa-caret-right"></i> Full name: ${fullName}. Total electricity bill: ${billTotal(
    electricityKW
  )} </p>`;
  document.getElementById("resultBT2").innerHTML = result2;
};

// PROCESS
// Function to calculate electricity bill
function billTotal(kw) {
  var total;
  if (kw < 50) {
    total = 500 * kw;
  } else if (kw >= 50 && kw < 100) {
    total = 500 * 50 + 650 * (kw - 50);
  } else if (kw >= 100 && kw < 200) {
    total = 500 * 50 + 650 * 50 + 850 * (kw - 100);
  } else if (kw >= 200 && kw < 350) {
    total = 500 * 50 + 650 * 50 + 850 * 100 + 1100 * (kw - 200);
  } else {
    total = 500 * 50 + 650 * 50 + 850 * 100 + 1100 * 150 + 1300 * (kw - 350);
  }
  return total.toLocaleString();
}

// ===================== EX 3 ===========================
const milion = 1000000;

document.getElementById("clickShow3").onclick = function () {
  //INPUT
  // Declare available variables which user inputed
  var name = document.getElementById("BT3_name").value;
  var totalIncome = parseFloat(
    document.getElementById("BT3_totalIncome").value
  );
  var person = parseInt(document.getElementById("BT3_dependentPerson").value);

  // call function and format number
  let totalIncomeAfterTax = new Intl.NumberFormat().format(
    incomeTax(totalIncome, person)
  );

  // RESULT
  // show result on the screen
  var result3 = `<p><i class="fa fa-caret-right"></i> Full name: ${name}. Total personal income tax: ${totalIncomeAfterTax} VND</p>`;
  document.getElementById("resultBT3").innerHTML = result3;
};

// PROCESS
// function to solve personal income tax
function incomeTax(income, person) {
  var total;
  total = income - 4 * milion - person * 1.6 * milion;

  var incomeAfterTax;
  if (total <= 60 * milion) {
    incomeAfterTax = total * 0.05;
  } else if (total > 60 * milion && total <= 120 * milion) {
    incomeAfterTax = total * 0.1;
  } else if (total > 120 * milion && total <= 210 * milion) {
    incomeAfterTax = total * 0.15;
  } else if (total > 210 * milion && total <= 384 * milion) {
    incomeAfterTax = total * 0.2;
  } else if (total > 384 * milion && total <= 624 * milion) {
    incomeAfterTax = total * 0.25;
  } else if (total > 624 * milion && total <= 960 * milion) {
    incomeAfterTax = total * 0.3;
  } else {
    incomeAfterTax = total * 0.35;
  }
  return incomeAfterTax;
}

// ===================== EX 4 ===========================
function display(that) {
  if (that.value == "b") {
    document.getElementById("BT4_connectNum").style.display = "block";
  } else {
    document.getElementById("BT4_connectNum").style.display = "none";
  }
}

document.getElementById("clickShow4").onclick = function () {
  // INPUT
  var customerType = document.getElementById("customer-select").value;
  var customerCode = document.getElementById("BT4_cstCode").value;
  var channelNumber = parseInt(document.getElementById("BT4_channelNum").value);
  var connectNum = document.getElementById("BT4_connectNum").value;

  // PROCESS
  if (customerType == "a") {
    individualCust(customerCode, channelNumber);
  } else if (customerType == "b") {
    businessCust(customerCode, channelNumber, connectNum);
  } else {
    alert("Please to choose type of Customer");
  }
};

// INDIVIDUAL function
function individualCust(custCode, channelNum) {
  // Declare varibales
  var billSolve = 4.5,
    basicSerFee = 20.5,
    preChannel = 7.5;

  // Process
  var total = billSolve + basicSerFee + channelNum * preChannel;

  //format result to clearly look
  var formatter;
  formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  }).format(total);

  // RESULT
  // show result on the screen
  var result4 = `<p><i class="fa fa-caret-right"></i> Customer'code: ${custCode}. Cable charge: ${formatter}</p>`;
  document.getElementById("result_BT4").innerHTML = result4;
}

// BUSINESS function
function businessCust(custCode, channelNum, connectNum) {
  // Declare varibales
  var billSolve = 15,
    basicSerFee = 75,
    preChannel = 50,
    moreConnectFee = 5;

  // Process
  var total;
  if (connectNum <= 10) {
    total = billSolve + basicSerFee + preChannel * channelNum;
  } else {
    total =
      billSolve +
      basicSerFee +
      moreConnectFee * (connectNum - 10) +
      preChannel * channelNum;
  }

  //format result to clearly look
  var formatter;
  formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  }).format(total);

  // RESULT
  // show result on the screen
  var result4 = `<p><i class="fa fa-caret-right"></i> Customer'code: ${custCode}. Cable charge: ${formatter}</p>`;
  document.getElementById("result_BT4").innerHTML = result4;
}
